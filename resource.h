﻿//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ 生成的包含文件。
// 供 openpicture.rc 使用
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_OPEN_PICTURE_DIALOG         102
#define IDR_MAINFRAME                   128
#define IDB_BITMAP1                     133
#define IDD_DIALOG1                     136
#define IDR_MENU1                       139
#define IDR_GIF1                        140
#define id_gif                          141
#define IDR_GIF2                        143
#define IDI_ICON1                       156
#define IDB_BITMAP2                     157
#define IDB_BITMAP3                     158
#define IDI_ICON2                       161
#define mbt_last                        1000
#define mbt_showPicture                 1001
#define mbt_next                        1002
#define met_pictureName                 1004
#define id_rect                         1006
#define id_returnPicture                1007
#define id_btDisplay                    1014
#define id_txt                          1021
#define id_picture                      1024
#define id_explain                      1025
#define ID_32772                        32772
#define last                            32773
#define bt_next                         32774
#define bt_last                         32775
#define pictureInform                   32776
#define ID_32777                        32777

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        162
#define _APS_NEXT_COMMAND_VALUE         32778
#define _APS_NEXT_CONTROL_VALUE         1026
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
