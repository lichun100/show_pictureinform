﻿// Cson.cpp: 实现文件
//
#include "pch.h"
#include "open_picture.h"
#include "Cson.h"
#include "afxdialogex.h"
#include "open_pictureDlg.h"
#include "check_rgb.h"
#include "gdiplus.h"
//using namespace Gdiplus;


#pragma comment(lib,"rgb_check.lib")


static int contorl = 0;
volatile BOOL m_bRun;
//HANDLE hMutex = NULL;     //互斥量

//char readBuffer[8000000] = { 0 };
//static CString ChooseFilePath;


// Cson 对话框

IMPLEMENT_DYNAMIC(Cson, CDialogEx)

Cson::Cson(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_DIALOG1, pParent)
//	, m_txt(_T(""))
{
//	m_hIcon = AfxGetApp()->LoadIconA(IDR_MAINFRAME);
}

Cson::~Cson()
{	

}


void Cson::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);

	DDX_Control(pDX, id_picture, m_picture);
}


BEGIN_MESSAGE_MAP(Cson, CDialogEx)
	ON_MESSAGE(WM_COUNT_MSG, &Cson::OnCountMsg)
	ON_WM_PAINT()
END_MESSAGE_MAP()





//用于将图片信息转化为文字信息的函数
void sonThread(LPVOID pParam) 
{
	Cson sDlg;
	CopenpictureDlg dlg;
	char readbuffer[20000000] = { 0 };
	dlg.get_data(&sDlg.rem_txt);
	BMP_RGB rgb_color;
	CString newName;
	int n = 0;
	memset(&rgb_color, 0, sizeof(BMP_RGB));

	if (red_bmp_data((char*)sDlg.rem_txt.GetBuffer(0), &rgb_color))    //调用函数获取图片信息
	{
//		GetDlgItem(id_txt)->SetWindowText("");
		//加处理
	}
	sDlg.rem_txt.ReleaseBuffer();
	n = sDlg.rem_txt.ReverseFind('.');              //查找rem_txt中的“.”
	newName = (sDlg.rem_txt).Left(n);                 //获取图片的名称除后缀名
	newName += _T(".txt");                     //创建一个信息的后缀名为.txt的文件名

	if (sDlg.findLikeFile(newName))                 //用于查看文件是否存在
	{
		CFile::Remove(newName);                //若文件存在则将文件删除
	}
	write_text_file(newName.GetBuffer(0), &rgb_color);
	newName.ReleaseBuffer();
//	*newNamex = (CString)newName;


	int nReadByte = 0;            //从文件中读取的字节数
//	char readBuffer[8000000] = { 0 };
	//以只读打开文件
	CFile file(newName, CFile::modeCreate | CFile::modeNoTruncate |
		CFile::modeReadWrite);
	file.SeekToBegin();
	while (true) {
		//读取文件内容到缓存中
		int nRet = file.Read(readbuffer + nReadByte, 10000);
		nReadByte += nRet;
		if (nRet < 10000)
			break;
	}
	//	this->SetWindowTextA(readBuffer);
	file.Close();

//	CWnd* pWnd = CWnd::FindWindowA(NULL, "Dialog");
//	if(pWnd)
	HWND hWnd = (HWND)pParam;
	SendMessage(hWnd, WM_COUNT_MSG, (WPARAM)(LPCTSTR)(CString)readbuffer, 0);

}


//寻找相同文件名的函数
BOOL Cson::findLikeFile(CString FileName) 
{
	HANDLE hfile;
	CString TxFilePath;
	CString AllFileName[100];
	CString TxFileName;
	WIN32_FIND_DATA lpFindFileData;
	int n = 0;
	int i = 0;

	n = FileName.ReverseFind('\\') + 1;
	TxFilePath = FileName.Left(n) + _T("*.txt");
	n = FileName.GetLength() - n;
	TxFileName = FileName.Right(n);
//	n = TxFileName.ReverseFind('.');
//	TxFileName = TxFileName.Right(n);

	hfile = FindFirstFile(TxFilePath, &lpFindFileData);
//	if (hfile == 0)
//		return ERROR;

	do
	{
		if (lpFindFileData.cFileName[0] == '.')
			continue;
		if (lpFindFileData.dwFileAttributes && FILE_ATTRIBUTE_DIRECTORY)
		{
			//			net_pictureName += lpFindFileData.cFileName;
			AllFileName[i] = lpFindFileData.cFileName;
			i++;
		}
		//			create_line(L, (char*)lpFindFileData.cFileName);
	} while (FindNextFile(hfile, &lpFindFileData));

	for (int j = 0; j < i; j++)
	{
		if (AllFileName[j] == TxFileName)
		{
			return TRUE;
		}
	}

	FindClose(hfile);
	return FALSE;
}


//初始化控件
BOOL Cson::OnInitDialog()
{
	CDialogEx::OnInitDialog();
	m_txt.SubclassDlgItem(id_txt, this);       //对话框中加入richtext



	// TODO:  在此添加额外的初始化

	return TRUE;  // return TRUE unless you set the focus to a control
				  // 异常: OCX 属性页应返回 FALSE
}

void Cson::OnPaint()
{
	CPaintDC dc(this); // device context for painting
					   // TODO: 在此处添加消息处理程序代码
					   // 不为绘图消息调用 CDialogEx::OnPaint()
	if (key == 0)
	{
		key = 1;
		CImage mim;
		mim.Load("./picture/timg.gif");
//		Image* mimage = ::new Image(L"E://picture/tt.gif");
		SetWindowPos(NULL, 0, 0, mim.GetWidth() + 15, mim.GetHeight() + 60, SWP_SHOWWINDOW);

		GetDlgItem(id_txt)->ShowWindow(FALSE);
		GetDlgItem(id_picture)->ShowWindow(TRUE);
		GetDlgItem(id_picture)->SetWindowPos(NULL, 0, 0, mim.GetWidth(),
			mim.GetHeight() + 60, SWP_SHOWWINDOW);
		mim.Destroy();

		keydown = 1;
		pSysMenu = GetSystemMenu(FALSE);
		ASSERT(pSysMenu != NULL);
		pSysMenu->EnableMenuItem(SC_CLOSE, MF_DISABLED);

		show_picture();
		fhThread = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)sonThread,
			GetSafeHwnd(), 0, &fThreadID);


	}
}



LRESULT Cson::OnCountMsg(WPARAM wParam, LPARAM lParam)
{
	CString newName;
	CString newFileName;
	CopenpictureDlg dlg;
	dlg.get_data(&newName);
//	char readBuffer[8000000] = { 0 };
	int n;

	n = newName.ReverseFind('\\') + 1;
	n = newName.GetLength() - n;
	newFileName = newName.Right(n);

	this->SetWindowTextA(newFileName);
//	this->SetDlgItemInt(id_txt, lParam);
	GetDlgItem(id_txt)->SetWindowText((LPCTSTR)wParam);       //将信息显示到窗口中
//	m_txt.PostMessageA(WM_VSCROLL, SB_BOTTOM, 0);            //将滚动条设置到末尾

	UpdateData(FALSE);

	CImage mim;
	mim.Load("./picture/timg.gif");
//	Image* mimage = ::new Image(L"E://picture/tt.gif");
	contorl = 1;

	WaitForSingleObject(hThread, INFINITE);
	CloseHandle(hThread);

	GetDlgItem(id_picture)->ShowWindow(FALSE);
	GetDlgItem(id_txt)->ShowWindow(TRUE);
	GetDlgItem(id_txt)->SetWindowPos(NULL, 0, 0, mim.GetWidth(),
		mim.GetHeight() + 20, SWP_SHOWWINDOW);

	mim.Destroy();

	dlg.control_key();

	pSysMenu->EnableMenuItem(SC_CLOSE, MF_ENABLED);
	keydown = 0;

	return 1;
}


void Cson::show_picture() 
{
	HDC hDC = GetDlgItem(id_picture)->GetDC()->GetSafeHdc();                   //不能被类的外部使用
	this->SetWindowTextA(_T("loading..."));

	m_bRun = TRUE;
	hThread = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)ThreadFunc,
		(HDC)hDC, 0, &ThreadID);

}


void ThreadFunc(HDC hDC)
{
	int choose_Run = 0;

	Image* mimage = Image::FromFile(L"./picture/timg.gif");
//	mimage = ::new Image(L"E://picture/timg.gif");
	//Image mage;
	//mimage.FromFile(L"11.gif");
	UINT count = 0;
	count = mimage->GetFrameDimensionsCount();                           //获取在该帧的图像对象的维数
	GUID* pDimensionIDs = (GUID*)new GUID[count];                        //
	mimage->GetFrameDimensionsList(pDimensionIDs, count);                //获取改帧的尺寸
	WCHAR strGuid[39];
	StringFromGUID2(pDimensionIDs[0], strGuid, 39);
	UINT frameCount = mimage->GetFrameCount(&pDimensionIDs[0]);          //获取在规定尺寸的帧的数量

	delete[]pDimensionIDs;

	int size = mimage->GetPropertyItemSize(PropertyTagFrameDelay);       //获取图像的大小（以字节为单位）
	PropertyItem* pItem = NULL;
	pItem = (PropertyItem*)malloc(size);
	mimage->GetPropertyItem(PropertyTagFrameDelay, size, pItem);         //从图片获取指定的属性项

	GUID    Guid = FrameDimensionTime;
	UINT fcount = 0;
///	contorl = 0;


	//下面最好用一个线程
	while (m_bRun)
	{
		Graphics gh(hDC); //hDC是外部传入的画图DC
		gh.DrawImage(mimage, 0, 0, mimage->GetWidth(), mimage->GetHeight());
		//重新设置当前的活动数据帧
		mimage->SelectActiveFrame(&Guid, fcount++);
		if (fcount == frameCount) //frameCount是上面GetFrameCount返回值
			fcount = 0;     //如果到了最后一帧数据又重新开始
	 //计算此帧要延迟的时间
		long lPause = ((long*)pItem->value)[fcount] * 10;
		Sleep(lPause);         //这里简单使用了sleep

		if (contorl == 1)
			break;
//			m_bRun = FALSE;
	}
	contorl = 0;

	DeleteDC(hDC);
	free(pItem);
	delete mimage;
	mimage = NULL;
}


void Cson::OnCancel()
{
	// TODO: 在此添加专用代码和/或调用基类
	CloseHandle(fhThread);
	DestroyWindow();
//	CDialogEx::OnCancel();
}

void Cson::PostNcDestroy()
{

	CDialog::PostNcDestroy();
	delete this;
}

BOOL Cson::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 在此添加专用代码和/或调用基类

	//禁用ESC
	if (pMsg->message == WM_KEYDOWN && pMsg->wParam == VK_ESCAPE && keydown == 1)
		return TRUE;

	if (pMsg->message == WM_KEYDOWN && pMsg->wParam == VK_RETURN && keydown == 1)
		return TRUE;

	if (pMsg->message == WM_SYSKEYDOWN && pMsg->wParam == VK_F4 && keydown == 1)
		return TRUE;

	return CDialogEx::PreTranslateMessage(pMsg);
}



