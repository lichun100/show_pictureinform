﻿
// open_pictureDlg.h: 头文件
//

#pragma once



// CopenpictureDlg 对话框
class CopenpictureDlg : public CDialogEx
{
// 构造
public:
	CopenpictureDlg(CWnd* pParent = nullptr);	// 标准构造函数



// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_OPEN_PICTURE_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持

private:
	HBITMAP hbmp;
	HANDLE hfile;


// 实现
protected:
	HICON m_hIcon;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
//	afx_msg void OnBnClickedlast();
	// 图片的名称
//	CString net_pictureName;
//	afx_msg void OnBnClickedshowpicture();
	CStatic m_rect;
	void show_Picture();    //自己定义的显示图片函数
	void find_fileName();   //在文件中选取一个图片文件
	void get_data(CString* data);
	void Get_pictureSize(int* bmwidth, int* bmheight);
	void init_picture();
//	CString m_etTxt;
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	CBitmap m_bmp;
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void Onlast();
	afx_msg void Onnext();
	afx_msg void Onpictureinform();
	afx_msg void OnChooseFile();
	CString m_explain;

	void control_key();
};
