﻿
#pragma once
#define WM_COUNT_MSG WM_USER+100


void ThreadFunc(HDC hDC);
// Cson 对话框
void sonThread(LPVOID pParam);

class Cson : public CDialogEx
{
	DECLARE_DYNAMIC(Cson)

public:
	Cson(CWnd* pParent = nullptr);   // 标准构造函数
	virtual ~Cson();


// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DIALOG1 };
#endif

private:
	CMenu* pSysMenu;
	int keydown = 0;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持
//	HICON m_hIcon;
	HANDLE hThread;       //线程句柄
	DWORD ThreadID;       //线程ID
	HANDLE fhThread;      //线程句柄
	DWORD fThreadID;      //线程ID

	afx_msg LRESULT OnCountMsg(WPARAM, LPARAM);     //消息处理函数

	DECLARE_MESSAGE_MAP()
public:
//	afx_msg void OnBnClickedbtdisplay();
	CString rem_txt;
//	CString m_txt;

	BOOL findLikeFile(CString FileName);
	void display_data();
//	CString m_txt;
	CRichEditCtrl m_txt;
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	int key = 1;
	CStatic m_picture;
	void show_picture();
	void PostNcDestroy();
	virtual void OnCancel();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
//	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
};
