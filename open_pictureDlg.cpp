﻿
// open_pictureDlg.cpp: 实现文件
//
#include <stdio.h>
#include "pch.h"
#include "framework.h"
#include "open_picture.h"
#include "open_pictureDlg.h"
#include "afxdialogex.h"
#include "Cson.h"

//#include "line.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#pragma warning(disable:4996)

//自定义全局变量
static CString AllFileName[100];
static CString FileLongthName;
static int choose = 0;
static int index = 0;
static int key = 0;


// 用于应用程序“关于”菜单项的 CAboutDlg 对话框

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
	DECLARE_MESSAGE_MAP()
public:
//	afx_msg void Onlast();
//	afx_msg void Onnext();
//	afx_msg void Onpictureinform();
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CopenpictureDlg 对话框


//更换图标
CopenpictureDlg::CopenpictureDlg(CWnd* pParent )//=nullptr
	: CDialogEx(IDD_OPEN_PICTURE_DIALOG, pParent)
	, m_explain(_T(""))
{
//	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_hIcon = AfxGetApp()->LoadIcon(IDI_ICON2);

}



void CopenpictureDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, id_rect, m_rect);
	DDX_Text(pDX, id_explain, m_explain);
}

BEGIN_MESSAGE_MAP(CopenpictureDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
//	ON_BN_CLICKED(mbt_showPicture, &CopenpictureDlg::OnBnClickedshowpicture)
	ON_WM_RBUTTONDOWN()
	ON_COMMAND(bt_last, &CopenpictureDlg::Onlast)
	ON_COMMAND(bt_next, &CopenpictureDlg::Onnext)
	ON_COMMAND(pictureInform, &CopenpictureDlg::Onpictureinform)
	ON_COMMAND(ID_32777, &CopenpictureDlg::OnChooseFile)
END_MESSAGE_MAP()


// CopenpictureDlg 消息处理程序

BOOL CopenpictureDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();


	// 将“关于...”菜单项添加到系统菜单中。

//	this->SetWindowTextA(AllFileName[index]);

	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != nullptr)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 设置此对话框的图标。  当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// TODO: 在此添加额外的初始化代码

	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void CopenpictureDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。  对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CopenpictureDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
//初始化图片
	if (AllFileName[index] == "")
	{
		show_Picture();

	}
	else
	{
//如果图片被覆盖，就重写
//		Invalidate(TRUE);
//		show_Picture();
		CClientDC dc(GetDlgItem(id_rect));            //在图形控件中显示
//		CPaintDC dc(GetDlgItem(id_rect));
		//	CDC dc;
		//	dc.m_hDC = ::GetDC(NULL);

		CRect rcclient;
		GetDlgItem(id_rect)->GetClientRect(&rcclient);
		CDC memdc;                                //	构造一个CDC对象
		memdc.CreateCompatibleDC(&dc);            // 	创建内存设备上下文，与另一个设备上下文匹配。可以用它在内存中准备图像
		CBitmap bitmap;
		bitmap.CreateCompatibleBitmap(&dc, rcclient.Width(), rcclient.Height());
		memdc.SelectObject(&bitmap);                           //	选择笔等GDI绘图对象

		CWnd::DefWindowProcA(WM_PAINT, (WPARAM)memdc.m_hDC, 0);                      //CDC对象使用的输出设备上下文

		CDC maskdc;
		maskdc.CreateCompatibleDC(&dc);
		CBitmap maskbitmap;
		maskbitmap.CreateBitmap(rcclient.Width(), rcclient.Height(), 1, 1, NULL);
		maskdc.SelectObject(&maskbitmap);
		maskdc.BitBlt(0, 0, rcclient.Width(), rcclient.Height(), &memdc,
			rcclient.left, rcclient.top, SRCCOPY);

		CBrush brush;
		brush.CreatePatternBrush(&m_bmp);
		dc.FillRect(rcclient, &brush);                     // 用指定画刷填充给定矩形


		dc.BitBlt(rcclient.left, rcclient.top, rcclient.Width(), rcclient.Height(),
			&memdc, rcclient.left, rcclient.top, SRCPAINT);              // 从指定设备上下文拷贝位图

		brush.DeleteObject();
		DeleteDC(memdc);
		DeleteDC(maskdc);
		DeleteObject(bitmap);
		DeleteObject(maskbitmap);
		DeleteDC(dc);

	}
}


//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CopenpictureDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


/*
HANDLE LoadImage(
	HINSTANCE hinst,     //若加载程序外部资源传NULL,否则一般传AfxGetInstanceHandle（）
	LPCTSTR lpszName,    //图片名称或全路径
	unit utype,        //图片类型:IMAGE_BITMAP或IMAGE_ICON或IMGAE_CURSOR
	int cxDesired,
	int cyDesired,
	UINT fuLoad      //一般为LR_DEFAULTCOLOR/LR_CREATEDIBSECTION
)
*/

/*
"←"：VK_LEFT
"↑"：VK_UP
"→"：VK_RIGHT
"↓"：VK_DOWN
*/
BOOL CopenpictureDlg::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 在此添加专用代码和/或调用基类
	if (WM_KEYDOWN == pMsg->message)
	{
		//按下←键
		if (VK_LEFT == pMsg->wParam)
		{
			if (FileLongthName != "")
			{
				Invalidate(TRUE);                       //刷新图片

				index--;
				if (index < 0)
				{
					do
					{
						++index;
					} while (AllFileName[index] != "");
					index--;
				}
				show_Picture();
				this->SetWindowTextA(AllFileName[index]);
				UpdateData(FALSE);
			}
			else
			{
				init_picture();
			}
		}
		//按下→键
		if (VK_RIGHT == pMsg->wParam)
		{
			if (FileLongthName != "")
			{
				Invalidate(TRUE);
				index++;
				if (AllFileName[index] == "")
					index = 0;
				show_Picture();
				this->SetWindowTextA(AllFileName[index]);
				UpdateData(FALSE);
			}
			else
			{
				init_picture();
			}
		}
	}
	return CDialogEx::PreTranslateMessage(pMsg);
}


//从位图文件中获得图片的高和宽
void CopenpictureDlg::Get_pictureSize(int* bmwidth, int* bmheight) {
	FILE* fp;
	CString path;
	if (FileLongthName != "")
	{
		CString lpFilePath;
		int n = FileLongthName.ReverseFind('\\') + 1;
		lpFilePath = FileLongthName.Left(n);
		path = lpFilePath;

		fp = fopen(path + AllFileName[index], "rb");
		fseek(fp, 18L, SEEK_SET);
		fread(bmwidth, 4, 1, fp);
		fread(bmheight, 4, 1, fp);
		fclose(fp);
	}
	else
	{
//		path = _T("E://picture/");
	}

}

//将文件路径传入子窗口
void CopenpictureDlg::get_data(CString* data) {

	CString path;
	if (FileLongthName != "")
	{
		int n = FileLongthName.ReverseFind('\\') + 1;
		path = FileLongthName.Left(n);

		*data = path + AllFileName[index];
	}
	else
	{
//		path = _T("E:\\picture\\");
		*data = "";
	}


}






//右键出现菜单
void CopenpictureDlg::OnRButtonDown(UINT nFlags, CPoint point)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	CMenu menu;                                 //定义下面要用到的cmenu对象
	menu.LoadMenuA(IDR_MENU1);                  //装载自定义的右键菜单 
	CMenu* pPopup = menu.GetSubMenu(0);        //获取第一个弹出菜单，所以第一个菜单必须有子菜单
	ClientToScreen(&point);                    //定义一个用于确定光标位置的位置  
	pPopup->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, point.x, point.y, this);         //在指定位置显示弹出菜单

	CDialogEx::OnRButtonDown(nFlags, point);
}

//显示图片
void CopenpictureDlg::show_Picture() {
	CString path;

	if (FileLongthName != "")
	{
		int n = FileLongthName.ReverseFind('\\') + 1;
		path = FileLongthName.Left(n);

		find_fileName();


		//获取图片的高和宽，将窗体设置合适的高和宽。
		int bmwidth, bmheight;
		Get_pictureSize(&bmwidth, &bmheight);
		//将控件设置为图像大小
		GetDlgItem(id_rect)->ShowWindow(TRUE);
		GetDlgItem(id_explain)->ShowWindow(FALSE);
		GetDlgItem(id_rect)->SetWindowPos(NULL, 0, 0, bmwidth , bmheight , SWP_SHOWWINDOW);
		if (m_bmp != "")
		{
			DeleteObject(m_bmp.Detach());             //	从CDC对象分离Windows设备上下文.
		}
		int cx, cy;
		if (hbmp != NULL)
			DeleteObject(hbmp);
		hbmp = (HBITMAP)::LoadImage(AfxGetInstanceHandle(), path + AllFileName[index], IMAGE_BITMAP,
			0, 0, LR_CREATEDIBSECTION | LR_LOADFROMFILE);
		m_bmp.Attach(hbmp);              //		把Windows设备上下文附加到这个CDC对象

		Get_pictureSize(&cx, &cy);

		CRect rect;

		GetDlgItem(id_rect)->GetWindowRect(&rect);     //将窗口矩形选中到picture控件上
		ScreenToClient(&rect);             //	将客户区选中到Picture控件表示的矩形区域内 

		GetDlgItem(id_rect)->MoveWindow(rect.left, rect.top,
			cx, cy, TRUE);                 //	将窗口移动到Picture控件表示的矩形区域  

		SetWindowPos(NULL, 0, 0, bmwidth + 17, bmheight + 39, SWP_SHOWWINDOW);
		

//		DeleteObject(hbmp);
	}
	else
	{
	//当没有图片时，显示一个空框
		GetDlgItem(id_rect)->ShowWindow(FALSE);
		GetDlgItem(id_explain)->ShowWindow(TRUE);
		SetWindowPos(NULL, 0, 0, 300, 300, SWP_SHOWWINDOW);
		GetDlgItem(id_explain)->SetWindowPos(NULL, 0, 0, 280, 260, SWP_SHOWWINDOW);
		GetDlgItem(id_explain)->SetWindowTextA(_T("1.按鼠标右键会弹出菜单。\n2.按键盘←键，→键能够切换图片。\n3.选择\
图片，可以从本地显示一张图片进行显示。\n4.图片信息会以文本的形式输出图片信息。"));
//		this->SetWindowTextA(_T("请通过右键，选择bmp图片"));
//		path = _T("E://picture/");
	}

}


void CopenpictureDlg::Onlast()
{
	// TODO: 在此添加命令处理程序代码
	if (FileLongthName != "")
	{
		Invalidate(TRUE);                       //刷新图片
		index--;
		if (index < 0)
		{
			do
			{
				++index;
			} while (AllFileName[index] != "");
			index--;
		}
		show_Picture();
		this->SetWindowTextA(AllFileName[index]);
		UpdateData(FALSE);
	}
	else
	{
		init_picture();
	}


}


void CopenpictureDlg::Onnext()
{
	// TODO: 在此添加命令处理程序代码
	if (FileLongthName != "")
	{
		Invalidate(TRUE);
		index++;
		if (AllFileName[index] == "")
			index = 0;
		show_Picture();
		this->SetWindowTextA(AllFileName[index]);
		UpdateData(FALSE);
	}
	else
	{
		init_picture();
	}

}

void CopenpictureDlg::control_key() 
{
	key = 0;
}



//进入子窗口
void CopenpictureDlg::Onpictureinform()
{
	// TODO: 在此添加命令处理程序代码

	if (FileLongthName != "")
	{
		//打开子窗口
		Cson* CFather = new Cson();
		HWND hwnd = CFather->GetSafeHwnd();
		if (key == 0)
		{
			key = 1;
			CFather->key = 0;
			CFather->Create(IDD_DIALOG1);
			CFather->ShowWindow(SW_SHOWNORMAL);
		}

	}
	else 
	{
		init_picture();
	}

//	PostNcDestroy();

/*
	if (IsWindow(hwnd))               //有就显示，不用重新创建
	{
		CFather->ShowWindow(SW_SHOWNORMAL);
	}
	else
	{
		CFather->Create(IDD_DIALOG1);
		CFather->ShowWindow(SW_SHOWNORMAL);
//		CGrandpa->ShowWindow(SW_HIDE);
	}
	UpdateData(FALSE);
*/
}


//获取文件内全部图片名称
void CopenpictureDlg::find_fileName() {
	//	L = init_line();
	int i = 0;
	CString lpFileName;
	WIN32_FIND_DATA lpFindFileData;
//此处判断可以省略。
	if (FileLongthName != "")
	{
		CString lpFilePath;
		int n = FileLongthName.ReverseFind('\\')+1;
		lpFilePath = FileLongthName.Left(n);
		lpFileName = lpFilePath + _T("*.bmp");
//		this->SetWindowTextA(lpFileName);
	}
	if (hfile != NULL)
		FindClose(hfile);

	hfile = FindFirstFile(lpFileName, &lpFindFileData);
	if (hfile == 0)
		return;
	for (int j = 0; j < 100; j++)
	{
		AllFileName[j] = "";
	}

	do
	{
		if (lpFindFileData.cFileName[0] == '.')
			continue;
		if (lpFindFileData.dwFileAttributes && FILE_ATTRIBUTE_DIRECTORY)
		{
			//			net_pictureName += lpFindFileData.cFileName;
			AllFileName[i] = lpFindFileData.cFileName;
			i++;
		}
		//			create_line(L, (char*)lpFindFileData.cFileName);
	} while (FindNextFile(hfile, &lpFindFileData));
	//	net_pictureName = AllFileName[0];

	if (choose == 1)
	{
		CString CrFileName;
		int x = FileLongthName.GetLength() - FileLongthName.ReverseFind('\\') - 1;
		CrFileName = FileLongthName.Right(x);
		this->SetWindowTextA(CrFileName);
		for (int n = 0; n < i; n++)
		{
			if (AllFileName[n] == CrFileName)
				index = n;
		}
		choose = 0;
	}

}

void CopenpictureDlg::OnChooseFile()
{
	// TODO: 在此添加命令处理程序代码
	init_picture();
}

void CopenpictureDlg::init_picture()
{
	CString strFileType;
	CString strPath;
	int n;
	BOOL isOpen = TRUE;                       //为TRUE显示打开对话框，false显示保存对话框
	CString defaultDir = "E:\\picture";       //指定默认的文件扩展名
	CString filename = "";                    //指定默认文件名
	CString filter = _T("(*.bmp)|*.bmp|(*.*)|*.*||");     //指名文件类型和相应的扩展名

	CFileDialog openFileDlg(isOpen, defaultDir, filename, OFN_HIDEREADONLY, filter);
	if (openFileDlg.DoModal() == IDOK)
	{
		CString filePath = openFileDlg.GetPathName();
		strPath = filePath;                  //获取文件路径
		n = strPath.GetLength() - strPath.ReverseFind('.') - 1;
		strFileType = strPath.Right(n);
		if (strFileType == "bmp")               //判断文件后缀名是否是bmp
		{
			FileLongthName = strPath;
		}
		else
		{
			strPath = "";
		}
	}
	if (strPath != "")
	{
		Invalidate(TRUE);                       //刷新图片
		choose = 1;
		show_Picture();
	}
	//	AllFileName[index] = filePath;
}


